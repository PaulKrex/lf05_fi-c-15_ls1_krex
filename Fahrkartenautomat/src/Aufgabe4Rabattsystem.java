import java.util.Scanner;
public class Aufgabe4Rabattsystem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner (System.in);
	    System.out.println("Gebe einen Preis ein: ");
	    double preis= scanner.nextDouble();
	    System.out.println("Gebe eine Anzahl: ");
	    int anzahl =scanner.nextInt();
	    double mwst = 1.19;
	    double a ;
	    double b ;
	    double c ;
	    
	    a = bisschenrabatt(preis,anzahl,mwst);
	    b = etwasrabatt(preis,anzahl,mwst);
	    c = geilerrabatt(preis,anzahl,mwst);
	    ausgabe(preis,anzahl,a,b,c,mwst);
		}
	    public static double bisschenrabatt(double preis, int anzahl, double mwst) {
	     return preis * anzahl * mwst * 0.9;
	    }
	    public static double etwasrabatt(double preis, int anzahl, double mwst) {
	     return preis * anzahl * mwst * 0.85;
	    }
	    public static double geilerrabatt(double preis, int anzahl, double mwst) {
	     return preis * anzahl * mwst* 0.8;
	    }
	  
	    public static void ausgabe(double preis, int anzahl, double bisschenrabatt, double etwasrabatt, double geilerrabatt, double mwst) {
	    	if (anzahl <= 100) {
	    		System.out.printf("Der Gesamte Lieferpreis betr�gt: %.2f" , bisschenrabatt);
	    	}
	    	if (anzahl > 100 && anzahl <= 500) {
	    		System.out.printf("Der Gesamte Lieferpreis betr�gt: %.2f" , etwasrabatt);
	    	}
	    	if (anzahl >= 500) {
	    		System.out.printf("Der Gesamte Lieferpreis betr�gt: %.2f" , geilerrabatt);
	    	
	    	}
	   
	    }
	   
	}
