import java.util.Scanner;
public class Quersumme {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		int zahl = scanner.nextInt();
		int summe = 0;
		while (zahl>= 1) {
			summe = summe + zahl%10;
			zahl = zahl/10;
		}
		System.out.println(summe);
	}

}
