import java.util.Scanner;
public class Volumenpyramide {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     Scanner scanner = new Scanner(System.in);
     System.out.println("Geben Sie eine Seitenl�nge ein: ");
     double a = scanner.nextDouble();
     System.out.println("Geben Sie eine H�he ein: ");
     double h = scanner.nextDouble();
     
     double v ;
     v = volumen(a,h);
     ausgabe(a,h,v);
     
	}
	 public static double volumen(double a, double h) {
     	return a * a * h/3 ;
     }
     public static void ausgabe(double a, double h, double v){
     	System.out.printf("Das Volumen von der Pyramide mit den Seitenl�ngen %.2f und H�he %.2f betr�gt: %.2f" , a , h , v);
     }
    
}