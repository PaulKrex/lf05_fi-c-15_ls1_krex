import java.util.Scanner;
public class Volumenkugel {

	public static void main(String[] args) {
     Scanner scanner = new Scanner(System.in);
     System.out.println("Geben Sie einen Radius ein: ");
     double r = scanner.nextDouble();
     double v ;
     v = volumen(r);
     ausgabe(r,v);
     
	}
	 public static double volumen(double r) {
     	return ((double)(4)/ (double)(3)) * r*r*r * 3.14 ;
     }
     public static void ausgabe(double r, double v){
     	System.out.printf("Das Volumen von der Kugel mit dem Radius %.2f betr�gt: %.2f" , r , v );
     }
    
}
