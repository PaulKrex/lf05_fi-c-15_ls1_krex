import java.util.Scanner;
public class CelsiusinKelvin {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie einen Startwert in Celsius ein: ");
		double startwert = scanner.nextInt();
		System.out.println("Bitte geben Sie einen Entwert in Celsius ein: ");
		double entwert = scanner.nextInt();
		System.out.println("Bitte geben Sie die Schrittweite in Celsius ein: ");
		double schrittweite = scanner.nextInt();
		if(startwert > entwert){
			System.out.println("Startwert muss kleiner als Entwert sein!");
			
		}
		while (startwert <= entwert) {
			System.out.printf("%10.2f�C%10.4s�F\n" , startwert, (startwert*1.8) + 30);
			startwert = startwert + schrittweite;
			
		}
		
		
	}

}
